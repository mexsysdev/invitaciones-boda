require("./bootstrap");
import moment from "moment";
import Toast from "vue-toastification";

window.Vue = require("vue").default;
Vue.prototype.$moment = moment;

import "vue-toastification/dist/index.css";
const options = {
    transition: "Vue-Toastification__bounce",
    maxToasts: 20,
    newestOnTop: true,
    position: "bottom-right",
    timeout: 5000,
    closeOnClick: true,
    pauseOnFocusLoss: true,
    pauseOnHover: true,
    draggable: true,
    draggablePercent: 0.6,
    showCloseButtonOnHover: false,
    hideProgressBar: false,
    closeButton: "button",
    icon: true,
    rtl: false,
};
Vue.use(Toast, options);

import GralLayout from "./components/GeneralLayout.vue";
// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

const app = new Vue({
    render: (h) => h(GralLayout),
    el: "#app",
});
