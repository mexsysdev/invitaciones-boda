import axios from "axios";
import he from "he";
const api = {
    event: {
        get: async (chunk) => {
            return axios.get(`/api/event/${he.decode(chunk)}`);
        },
        images: {
            getBannerImages: async (event_id) => {
                return axios.get(`/api/event/${event_id}/images/banner`);
            },
            getEventGallery: async (event_id) => {},
        },
        guests: {
            get: async (code) => {
                return axios.get(`/api/event/guests/${code}`);
            },
            confirmAttendance: async (guest) => {
                return axios.post(
                    `/api/event/guests/${guest.id}/attend`,
                    JSON.stringify(guest)
                );
            },
        },
    },
};

export default api;
