<?php

use App\Http\Controllers\EventController;
use App\Http\Controllers\EventGuestController;
use App\Http\Controllers\ImagesController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/event/{chunk}', function ($chunk) {
    return EventController::show($chunk);
});
Route::get('/event/guests/{code}',function($code){
    return EventGuestController::show($code);
});
Route::post('/event/guests/{id}/attend',[EventGuestController::class,'update']);
Route::get('/event/{eventId}/images/banner',[ImagesController::class,'index']);