<?php

use App\Models\event;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('banner_images')) {
            Schema::create('banner_images', function (Blueprint $table) {

                $table->id()->autoIncrement()->unsigned();
                $table->string('url', 200)->nullable(false);
                $table->string('event_id');
                $table->foreign('event_id')->references('id')->on('event')->cascadeOnUpdate()->cascadeOnDelete();
                $table->boolean('active')->default(true);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner_images');
    }
};
