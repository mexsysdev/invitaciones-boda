<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_guest', function (Blueprint $table) {
            $table->uuid('id')->nullable(false)->default(DB::raw('(UUID())'))->primary();
            $table->string('code',7)->nullable(false);
            $table->string('nombre',50)->nullable(false);
            $table->boolean('lactosa')->default(false);
            $table->boolean('nueces')->default(false);
            $table->boolean('mariscos')->default(false);
            $table->boolean('vegano')->default(false);
            $table->boolean('gluten')->default(false);
            $table->string('otra_alergia',50)->default(null);
            $table->boolean('confirmado')->default(false)->nullable(false);
            $table->string('event_id',50)->nullable(false);
            $table->foreign('event_id')->references('id')->on('event');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_guest');
    }
};
