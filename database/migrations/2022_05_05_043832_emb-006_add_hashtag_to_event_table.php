<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('event') && !Schema::hasColumn('event','hashtag'))
            Schema::table('event', function (Blueprint $table) {
                $table->string('hashtag', 50)->nullable(true)->default(null);
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('event') && Schema::hasColumn('event','hashtag'))
            Schema::table('event', function (Blueprint $table) {
                $table->dropColumn('hashtag');
            });
    }
};
