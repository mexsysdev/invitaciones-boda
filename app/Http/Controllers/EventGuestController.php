<?php

namespace App\Http\Controllers;

use App\Models\EventGuest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class EventGuestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EventGuest  $eventGuest
     * @return \Illuminate\Http\Response
     */
    public static function show($eventGuest)
    {
        return EventGuest::getEventGuests($eventGuest);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EventGuest  $eventGuest
     * @return \Illuminate\Http\Response
     */
    public function edit(EventGuest $eventGuest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EventGuest  $eventGuest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $guestId)
    {
        $guest = EventGuest::find($guestId);
        $jsonData = $request->json()->all();
        $guest->lactosa = $jsonData['lactosa'];
        $guest->nueces = $jsonData['nueces'];
        $guest->mariscos = $jsonData['mariscos'];
        $guest->vegano = $jsonData['vegano'];
        $guest->gluten = $jsonData['gluten'];
        $guest->otra_alergia = $jsonData['otra_alergia'];
        $guest->confirmado = true;
        $guest->save();
        return response()->make("ok",200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EventGuest  $eventGuest
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventGuest $eventGuest)
    {
        //
    }
}
