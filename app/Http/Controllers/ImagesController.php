<?php

namespace App\Http\Controllers;

use App\Models\banner_images;
use Illuminate\Http\Request;

class ImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($eventId, $bannerImg = true)
    {
        if ($bannerImg)
            return banner_images::where('active', true)->where('event_id', $eventId)->get()->map(function ($el) {
                return ['image' => $el->url];
            });
        else return [];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\banner_images  $banner_images
     * @return \Illuminate\Http\Response
     */
    public function show(banner_images $banner_images)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\banner_images  $banner_images
     * @return \Illuminate\Http\Response
     */
    public function edit(banner_images $banner_images)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\banner_images  $banner_images
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, banner_images $banner_images)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\banner_images  $banner_images
     * @return \Illuminate\Http\Response
     */
    public function destroy(banner_images $banner_images)
    {
        //
    }
}
