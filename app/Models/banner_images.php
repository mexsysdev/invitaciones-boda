<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class banner_images extends Model
{
    use HasFactory;
    public $timestamps = false;

    public function event()
    {
        return $this->belongsTo(event::class,'event_id');
    }
}
