<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class event extends Model
{
  use HasFactory;
  protected $table = 'event';
  protected $casts = [
    'id' => 'string'
  ];

  public function banner_images()
  {
    return $this->hasMany(\App\Models\banner_images::class);
  }
}
