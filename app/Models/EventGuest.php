<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventGuest extends Model
{
    use HasFactory;
    protected $table = 'event_guest';
    protected $casts = [
        'id' => 'string'
    ];
    public $incrementing = false;
    protected $fillable = ['id'];
    public $timestamps = false;
    public static function getEventGuests($guest_code)
    {
        return self::where('code', $guest_code)
            // ->where('confirmado', false)
            ->get();
    }
}
